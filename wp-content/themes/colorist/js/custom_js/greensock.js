var anims = {
  servicesStatus: false,
  services: function(){
    const tl = new TimelineMax();
          tl.staggerTo($("#services .icon"), 1.1, {autoAlpha: 1, top: "0px", ease: Power2.easeInOut}, 0.05)
          .staggerTo($("#services .services-content"), .9, {autoAlpha: 1, top: "0px", ease: Power2.easeInOut}, 0.05, "-=0.7");
          this.servicesStatus = true;
  }
}

function checkAnim($viewElem, animElem) {
    if (helper.isElementInViewport($viewElem[0])||helper.isElementInViewport($viewElem[1])) {
      console.log($viewElem);
      anims[animElem]();
    }
}

$(function() {
	$(window).scroll(function(){
		if (!anims.servicesStatus){
      checkAnim([$("#services .icon"),$("#services .btn")], "services");
    }
	});

  /////////////////////////////////////////////
  ///////* HEADER [ WE TARGET 'X' ] */////////
  ///////////////////////////////////////////
  var headerQuotes = [ "sales", "traffic", "conversion", "reach" ];
  var numHeaderQuotes = headerQuotes.length;
  var index = 0;
  var selection = $("#changeWord");
  selection.html(headerQuotes[index]);

  setInterval(function() {
    selection.fadeOut( "slow", function() {
      index = (index + 1) % numHeaderQuotes;
      selection.html(headerQuotes[index]);
      selection.fadeIn( "slow");
    });
  }, 2000);

});



/////////////////////////////////////////////
//////* ONE DESIGN, FOR ALL DEVICES *///////
///////////////////////////////////////////
$(document).ready(function(){

    function checkSVGSupport(){
      let animation = document.querySelector(".replaced-svg");
      if (animation!==null) {
        clearInterval(timer);
        (function(){var a=function(a){return document.querySelector(a)},t=document.querySelectorAll(".monitorContentGroup path");a(".monitorGroup");var f=a(".monitorScreen"),g=a(".monitorLogo"),h=a(".monitorStand"),u=a(".monitorStandShadow"),c=a(".monitorBottom"),v=a(".monitorEdge"),C=document.querySelectorAll(".laptopContentGroup path"),w=a(".laptopContentGroup"),x=a(".laptopGroup"),d=a(".laptopScreen"),k=a(".laptopEdgeLeft"),l=a(".laptopEdgeRight"),m=a(".laptopTrackpad"),n=a(".laptopBase");document.querySelectorAll(".tabletContentGroup path");
        var e=a(".tabletContentGroup"),b=a(".tabletGroup"),p=a(".tabletButton"),q=a(".tabletCamera"),y=a(".tabletScreen"),r=a(".phoneGroup"),z=a(".phoneButton"),A=a(".phoneCamera"),a=a(".phoneSpeaker");TweenMax.set([c],{transformOrigin:"50% 100%"});TweenMax.set([h,n,y],{transformOrigin:"50% 0%"});TweenMax.set([g,f,d,m,b,x,p,q,e,z,A,a,w,r],{transformOrigin:"50% 50%"});TweenMax.set([k,l],{transformOrigin:"0% 100%"});TweenMax.set(b,{rotation:-90});var B=new TimelineMax({repeat:-1,delay:1,yoyo:!1,paused:!1});
        B.timeScale(3);B.from(c,1,{scaleY:0,ease:Power1.easeOut}).from(h,1,{y:-70,ease:Power1.easeOut},"-=1").from(u,.5,{alpha:0,ease:Power1.easeIn},"-=0.5").from(v,1,{y:330},"-=0.25").from(f,2,{y:330,ease:Power1.easeOut},"-=1").staggerFrom(t,1,{scaleX:0},.1,"-=0.51").from(g,1,{scale:0,ease:Back.easeOut.config(2)}).staggerTo(t,1,{alpha:0,delay:2},.1).to(f,1,{y:330,ease:Power1.easeIn},"-=1").to(v,1,{y:330,ease:Power1.easeIn},"-=0.75").to(c,1,{scaleX:.69,y:-23}).to(c,1,{scaleY:.45,alpha:1},"-=1").set(c,{alpha:0}).to(g,
        .5,{scale:0,ease:Back.easeIn},"-=1").to(h,1,{y:-120},"-=1.5").to(u,.5,{alpha:0},"-=1.5").from(n,1,{alpha:0},"-=1").from(m,1,{scaleX:0},"-=1").from(d,1,{scale:0,ease:Back.easeOut.config(.5)}).from(k,2,{skewX:-40,scaleY:0,ease:Power3.easeOut},"-=2").from(l,2,{skewX:40,scaleY:0,ease:Power3.easeOut},"-=2").staggerFrom(C,1,{scaleX:0},.1).to(m,.3,{alpha:0,delay:2}).to(d,1,{scaleX:.67}).to(d,1,{scaleY:.8},"-=1").to(w,1,{alpha:0,scale:.5},"-=1").to(n,1,{y:-20,scaleX:0},"-=1").to(k,1,{x:40,transformOrigin:"50% 50%",
        scaleY:.85},"-=1").to(l,1,{x:-40,transformOrigin:"50% 50%",scaleY:.85},"-=1").set(x,{alpha:0}).from(b,1,{scale:1.1,alpha:0},"-=1").to(b,2,{rotation:0,delay:2,ease:Anticipate.easeOut}).staggerFrom([p,q],.5,{scale:0,ease:Back.easeOut},.1,"-=1").from(e,2,{rotation:90,scaleX:1.33,scaleY:.8,ease:Power3.easeInOut},"-=0").to([p,q],.5,{scale:0,delay:2}).to(b,1,{scaleX:.45}).to(b,1,{scaleY:.7},"-=1").to(e,1,{y:-5},"-=1").to(y,.5,{scaleY:.92,y:4},"-=0.5").set(r,{alpha:1}).set([b,e],{alpha:0}).staggerFrom([z,
        A,a],1,{scale:0,ease:Back.easeOut},.1).to(r,2,{y:80,delay:2,ease:Back.easeIn.config(2)});TweenMax.set("svg",{visibility:"visible"})})();
      }
    }
    checkSVGSupport();
    var timer = setInterval(checkSVGSupport, 1);
});





/////////////////////////////////////////////
//////////////* CONTACT FORM *//////////////
///////////////////////////////////////////

// get all data in form and return object
function getFormData() {
  var elements = document.getElementById("contactEnquire").elements; // all form elements
  var fields = Object.keys(elements).map(function(k) {
    if(elements[k].name !== undefined) {
      return elements[k].name;
    // special case for Edge's html collection
    }else if(elements[k].length > 0){
      return elements[k].item(0).name;
    }
  }).filter(function(item, pos, self) {
    return self.indexOf(item) == pos && item;
  });
  var data = {};
  fields.forEach(function(k){
    data[k] = elements[k].value;
    var str = ""; // declare empty string outside of loop to allow
                  // it to be appended to for each item in the loop
    if(elements[k].type === "checkbox"){ // special case for Edge's html collection
      str = str + elements[k].checked + ", "; // take the string and append
                                              // the current checked value to
                                              // the end of it, along with
                                              // a comma and a space
      data[k] = str.slice(0, -2); // remove the last comma and space
                                  // from the  string to make the output
                                  // prettier in the spreadsheet
    }else if(elements[k].length){
      for(var i = 0; i < elements[k].length; i++){
        if(elements[k].item(i).checked){
          str = str + elements[k].item(i).value + ", "; // same as above
          data[k] = str.slice(0, -2);
        }
      }
    }
  });
//  console.log(data);
  return data;
}

function handleFormSubmit(event) {  // handles form submit withtout any jquery
  event.preventDefault();           // we are submitting via xhr below
  var data = getFormData();         // get the values submitted in the form

  /* OPTION: Remove this comment to enable SPAM prevention, see README.md
  if (validateHuman(data.honeypot)) {  //if form is filled, form will not be submitted
    return false;
  }
  */

  var url = event.target.action;  //
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  // xhr.withCredentials = true;
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onreadystatechange = function() {
//      console.log( xhr.status, xhr.statusText )
//      console.log(xhr.responseText);
      $('#getInTouch').modal('hide');
      return;
  };
  // url encode form data for sending as post data
  var encoded = Object.keys(data).map(function(k) {
      return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
  }).join('&')
  xhr.send(encoded);
}

function loaded() {
  // bind to the submit event of our form
  var form = document.getElementById('contactEnquire');
  form.addEventListener("submit", handleFormSubmit, false);
};
document.addEventListener('DOMContentLoaded', loaded, false);
