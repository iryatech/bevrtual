<?php /* Template Name: bookdemo */ ?>
<?php 
    get_header();
?>
<div class="container_radius">
        <div class="row form_and_brand">
            <div class="row heading_content">
                <div class="col-xs-12 heading_radius">
                    Free 1st month designed &amp; developed website<br> + 1 month free google or facebook ads
                </div>
                <div class="col-xs-12 subheading_radius">
                    No contract,no commitment 
                </div>
                <div class="col-xs-12 subheading_radius">
                    You’ll own the design &amp; developed website 
                </div>
                <div class="col-xs-12 subheading_radius">
                    Now this is an opportunity to bevrtual!
                </div>
            </div>
            <div class="row radius_contact_row">
                <div class="col-md-6 col-xs-12 radius_contact_form">
                    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.14/iframeResizer.min.js"></script><iframe src="https://hello.dubsado.com:443/public/form/view/5a8670607f98f41253f4c5c6" frameborder="0" width="100%" height="750"></iframe><script type="text/javascript">setTimeout(function(){iFrameResize({checkOrigin: false});},30);</script>
                </div>
                <div class="col-md-6 col-xs-12 radius_logo_brands">
                    <div class="row">
                        <div class="col-xs-12 logo_brands">
                            <img src="../images/All-Logos.png" />
                        </div>
                        <div class="col-xs-12 logo_brands_brief">
                            Some of the world’s most respected companies use the platform we design and develop your website on
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row radius_icons">
            <div class="col-md-6 col-xs-12 icon_block icon_blockone">
                <div class="row">
                    <div class="col-md-3 col-xs-3 icon_icon">
                        <!--icon here-->
                        <span class="glyphicon glyphicon-user"></span>
                    </div>
                    <div class="col-md-9 col-xs-9">
                        <div class="col-xs-12 icon_heading">
                            Website Designed &amp; Developed Exclusively by Our Team For your brand
                        </div>
                        <div class="col-xs-12 icon_content">
                            Before starting the website design &amp; development we will have a one-on-one consultation with you to make sure we understand your needs and goals along with your target market.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 icon_block icon_blocktwo">
                <div class="row">
                    <div class="col-md-3 col-xs-3 icon_icon">
                        <!--icon here-->
                        <span class="glyphicon glyphicon-check"></span>
                    </div>
                    <div class="col-md-9 col-xs-9">
                        <div class="col-xs-12 icon_heading">
                            Ads Designed &amp; Developed by Our Team For your Brand 
                        </div>
                        <div class="col-xs-12 icon_content">
                            The purpose of any business is to make money, so our digital marketing team and designers will design an effective s  pairing your website with a strategic marketing plan is
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 icon_block icon_blockthree">
                <div class="row">
                    <div class="col-md-3 col-xs-3 icon_icon">
                        <!--icon here-->
                        <span class="glyphicon glyphicon-fire"></span>
                    </div>
                    <div class="col-md-9 col-xs-9">
                        <div class="col-xs-12 icon_heading">
                            Why Such a flabbergasting/ phenomenal offer? 
                        </div>
                        <div class="col-xs-12 icon_content">
                            Our Goal in 2018 is to help 10,000 small businesses launch a remarkable website that performs and gets results! From 11 years of experience we know that a well-designed website paired with strategic marketing is the only way to have a successful launch (or get results).
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 icon_block icon_blockfour">
                <div class="row">
                    <div class="col-md-3 col-xs-3 icon_icon">
                        <!--icon here-->
                        <span class="glyphicon glyphicon-fire"></span>
                    </div>
                    <div class="col-md-9 col-xs-9">
                        <div class="col-xs-12 icon_heading">
                            Why Such a flabbergasting/ phenomenal offer? 
                        </div>
                        <div class="col-xs-12 icon_content">
                            Our Goal in 2018 is to help 10,000 small businesses launch a remarkable website that performs and gets results! From 11 years of experience we know that a well-designed website paired with strategic marketing is the only way to have a successful launch (or get results).
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row quote_radius">
            <div class="col-xs-12 quote">
                “With the help and direction of my ProjectManger and team at BeVrtual I was able to achieve the look and vibe that I so desperately needed for my business. I would not classify myself as a creative person yet I always seem to be very particular with what I am looking for, after all my business is my baby! The branding team took their time to really understand the balance between what I wanted and what my business NEEDED. I had my website up and running with time to spare. The entire process was thorough and seamless in execution at a monthly affordable rate and on top of that I OWN my website!”
            </div>
            <div class="col-xs-12 writer">
                BRIAN H.
            </div>
        </div>
    </div>

<div class="button_show">show</div>
<div class="black_bg hide"></div>
<div class="container bookdemo_container hide"> 
    <div class="close_button">X</div>
    <div class="row bookdemo_container_row">
        <div class="col-xs-12 bookdemo_tabs">
            <div class="row bookdemo_tabs_container">
                <div class="col-xs-4 options_tab option_one">Tab1</div>
                <div class="col-xs-4 options_tab option_two">Tab2</div>
                <div class="col-xs-4 options_tab option_three">Tab3</div>
            </div>
        </div>
        <div class="row bookdemo_content">
            <form method="post" class="democlass" id="bookdemoform" >
                <div class="col-xs-12 options_content option_one_content">
                    <div class="row form_one">
                        <div class="col-xs-12">
                            <label for="">Do you have an existing website?</label>
                        </div>
                        <div class="col-xs-12">
                            <select class="form_one_required website_ask" name="website_ask" required="" tabindex="0" required>
                                <option value="">Choose</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                        <div class="col-xs-12 website_url">
                            <label for="">If Yes, Current Website URL</label>
                        </div>
                        <div class="col-xs-12 website_url">
                            <input class="website_url_val" type="text" name="website_url" placeholder="Website url..." />
                        </div>
                        <div class="col-xs-12">
                            <label for="">Industry</label>
                        </div>
                        <div class="col-xs-12">
                            <select class="form_one_required" name="industry" required="" tabindex="0" required>
                                <option value="">Select</option>
                                <option value="Auto" >Auto</option>
                                <option value="Leisure" >Leisure</option>
                                <option value="Finance" >Finance</option>
                                <option value="Fitness" >Fitness</option>
                                <option value="Improvement" >Improvement</option>
                                <option value="Medical" >Medical</option>
                                <option value="Profit" >Profit</option>
                                <option value="Other" >Other</option>
                                <option value="Professional" >Professional</option>
                                <option value="Estate" >Estate</option>
                                <option value="Restaurant" >Restaurant</option>
                                <option value="Retail" >Retail</option>
                                <option value="Salon" >Salon</option>
                                <option value="Spa" >Spa</option>
                                <option value="Travel" >Travel</option>
                            </select>
                        </div>
                        <div class="col-xs-12">
                            <button class="submit_form_one">Continue > </button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 options_content option_two_content" style="display:none">
                    <div class="row form_two">
                        <div class="col-xs-12">
                            <label for="">What services are you interested in?</label>
                        </div>
                        <div class="col-xs-12">
                            <select class="form_two_required ask_service" name="service_need" required="" tabindex="0" required>
                                <option value="">Select</option>
                                <option value="New Website">New Website</option>
                                <option value="Website Redesign">Website Redesign</option>
                                <option value="Online Store">Online Store</option>
                                <option value="MLS Search (Real Estate)">MLS Search (Real Estate)</option>
                                <option value="Appointment Booking">Appointment Booking</option>
                                <option value="Events Calendar">Events Calendar</option>
                                <option value="Online Food Ordering – Delivery or Takeout">Online Food Ordering – Delivery or Takeout</option>
                                <option value="Client Portal">Client Portal</option>
                                <option value="Blog Posting">Blog Posting</option>
                                <option value="Full or Partial Copywriting">Full or Partial Copywriting</option>
                                <option value="Social Media Integration">Social Media Integration</option>
                                <option value="other">Other</option>
                                <option value="I’m Not Sure – Lets Discuss">I’m Not Sure – Lets Discuss</option>
                            </select>
                        </div>
                        <div class="col-xs-12 other_service">
                            <label for="">Other service</label>
                        </div>
                        <div class="col-xs-12 other_service">
                            <input class="other_service_val" type="text" name="other_service" />
                        </div>
                        <div class="col-xs-12">
                            <button class="submit_form_two">last step ></button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 options_content option_three_content" style="display:none">
                    <div class="row form_three">
                        <div class="col-xs-12">
                            <div class="col-xs-12">
                                <label for="">Name</label>
                            </div>
                            <div class="col-xs-12">
                                <input class="form_three_required" type="text" name="name" required />
                            </div>
                            <div class="col-xs-12">
                                <label for="">Email</label>
                            </div>
                            <div class="col-xs-12">
                                <input class="form_three_required" type="email" name="email" required />
                            </div>
                            <div class="col-xs-12">
                                <label for="">Number</label>
                            </div>
                            <div class="col-xs-12">
                                <input class="form_three_required" type="text" name="number" required />
                            </div>
                            <div class="col-xs-12">
                                <label for="">Any additional Information you would like to include?</label>
                            </div>
                            <div class="col-xs-12">
                                <textarea name="additional_info"></textarea>
                            </div>
                            <div class="col-xs-12">
                                <input type="submit" />
                                <button name="submit_form" class="submit_form_three">Send my free proposal</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
    get_footer();
exit;
?>