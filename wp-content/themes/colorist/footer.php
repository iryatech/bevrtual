<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Colorist
 */
?>
		</div> <!-- .container -->
	</div><!-- #content -->

	<?php do_action('colorist_before_footer'); ?>

<footer class="footer">
  <div class="footer-top section">
    <div class="container" style="max-width: 1170px;">
      <div class="row">
        <div class="footer-col col-md-5">
          <h5>Where we come from</h5>
          <p>bevrtual started with a team of individuals that successfully developed and implemented ecommerce websites for their businesses and conquered the Digital challenges when expanding worldwide. Soon after bevrtual was founded, we scouted small and large businesses struggling in branding, website design, development, SEO, marketing, launching, and maintaining marketplaces such as Amazon &amp; Ebay. We helped them get to their vision, in HALF the time it takes the average Digital Agency. How? Simply, because we are above average.</p>
                <h5>
		              <a href="/privacy-policy">
                    PRIVACY POLICY
            </a>
                </h5>

          <h5>CONTACT</h5>
          <p>
            Call Toll Free:<br>1.800.604.2526
          </p>
          <p>Copyright &copy; 2017 Be Vrtual, Inc. All Rights Reserved</p>
        </div>
        <div class="footer-col col-md-3 col-md-offset-1">
          <h5>Locations</h5>
          <div class="address">
            <h6>LA (Headquarters)</h6>
            <p>1800 Century Park East 600<br>Los Angeles, CA 90067</p>
          </div>
          <div class="address">
            <h6>San Francisco</h6>
            <p>71 Stevenson Street 400<br>San Francisco, CA 94105</p>
          </div>
        </div>
        <div class="footer-col col-md-3">
          <h5>&nbsp;</h5>
          <div class="address">
            <h6>Austin</h6>
            <p>2021 Guadalupe Street 260<br>Austin, TX 78705</p>
          </div>
          <div class="address">
            <h6>Manhattan</h6>
            <p>415 Madison Avenue fl 14<br>Manhattan, NY 10017</p>
          </div>
          <div class="address">
            <h6>INTERNATIONAL</h6>
            <p>Canada, UK</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
	<?php do_action('colorist_after_footer'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
		$(document).ready(function(){
				function adjust_height(){
						var banner_vid_height = $('.bannner_videos').height();
						$("#vid_banner").height(banner_vid_height);
				}
				adjust_height();
				$(window).resize(function(){
						adjust_height();
				});

		});
</script>

<?php

global $wpdb;

if ( isset( $_POST["website_ask"] ) ) {
    echo "posted";
        $website_ask = $_POST["website_ask"];
        $website_url = $_POST["website_url"];
        $industry = $_POST["industry"];
        $service_need = $_POST["service_need"];
        $other_service = $_POST["other_service"];
        $name = $_POST["name"];
        $email = $_POST["email"];
        $number = $_POST["number"];
        $additional_info = $_POST["additional_info"];
    $table = $wpdb->prefix . "bookdemo"; 
    $wpdb->insert( 
        $table, 
        array( 
            'website_ask' => $website_ask,
            'website_url' => $website_url,
            'industry' => $industry,
            'service_need' => $service_need,
            'other_service' => $other_service,
            'name' => $name,
            'email' => $email,
            'number' => $number,
            'additional_info' => $additional_info
        ), 
        array( 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%s', 
            '%d', 
            '%s' 
        ) 
    );
    echo "done";
}
?>
<?php    
        wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array ( 'jquery' ), 1.1, true);
    ?>
<script type="text/javascript">
    $(document).on('ready', function() {
        
        
     /* $(".lazy").slick({
        lazyLoad: 'ondemand', // ondemand progressive anticipated
        infinite: true
      });*/
        $('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: true,
          centerMode: true,
          focusOnSelect: true
        });
        
        $(".slick-current").prev().addClass("slick-active_custom");
        $(".slick-current").next().addClass("slick-active_custom");
        $('.slick-slide').click(function(){
            $(".slick-slide").each(function(){
                $(this).removeClass('slick-active_custom');
            });
            $(".slick-current").prev().addClass("slick-active_custom");
            $(".slick-current").next().addClass("slick-active_custom");
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.option_one').click(function(){
            if( $(this).hasClass('filled') ){
                $('.options_content').css("display" , "none");
                $('.option_one_content').css("display" , "block");
            }
            
        });
        $('.option_two').click(function(){
            if( $(this).hasClass('filled') ){
                $('.options_content').css("display" , "none");
                $('.option_two_content').css("display" , "block");
            }
        });
        $('.option_three').click(function(){
            if( $(this).hasClass('filled') ){
                $('.options_content').css("display" , "none");
                $('.option_three_content').css("display" , "block");
            }
        });
        $('.website_ask').change(function(){
            if( $(this).val() == 'yes'){
                $('.website_url').css("display" , "block");
            }else{
                $('.website_url').css("display" , "none");
            }
        });
        $('.submit_form_one').click(function(){
            var error = 0;
            $('.form_one_required').each(function(){
                if( $(this).val() == '' ){
                    error = 1;
                    $(this).css("border" , "2px solid red");
                }else{
                    $(this).css("border" , "2px solid green");
                }
            });
            if( $('.website_ask').val() == 'yes'){
                if( $('.website_url_val').val() == '' ){
                    error = 1;
                    $('.website_url_val').css("border" , "2px solid red");
                }else{
                    $('.website_url_val').css("border" , "2px solid green");
                }
            }
                if(error == 0){
                    $('.options_content').css("display" , "none");
                    $('.option_two_content').css("display" , "block");
                    $('.options_tab.option_one').addClass('filled');
                }else{
                    $('.options_tab.option_one').removeClass('filled');
                }
        });
        $('.ask_service').change(function(){
            if( $(this).val() == 'other'){
                $('.other_service').css("display" , "block");
            }else{
                $('.other_service').css("display" , "none");
            }
        });
        $('.submit_form_two').click(function(){
            var error = 0;
            $('.form_two_required').each(function(){
                if( $(this).val() == '' ){
                    error = 1;
                    $(this).css("border" , "2px solid red");
                }else{
                    $(this).css("border" , "2px solid green");
                }
            });
            
            if( $('.ask_service').val() == 'other'){
                if( $('.other_service_val').val() == '' ){
                    error = 1;
                    $('.other_service_val').css("border" , "2px solid red");
                }else{
                    $('.other_service_val').css("border" , "2px solid green");
                }
            }
                if(error == 0){
                    $('.options_content').css("display" , "none");
                    $('.option_three_content').css("display" , "block");
                    $('.options_tab.option_two').addClass('filled');
                }else{
                    $('.options_tab.option_two').removeClass('filled');
                }
        });
        $('.submit_form_three').click(function(){
            var error = 0;
            $('.form_three_required').each(function(){
                if( $(this).val() == '' ){
                    error = 1;
                    $(this).css("border" , "2px solid red");
                }else{
                    $(this).css("border" , "2px solid green");
                }
            });
            if(error == 0){
                
                //$('#bookdemoform').submit();
            }
            
        });
        $('.button_show').click(function(){
            $('.black_bg').removeClass("hide");
            $('.bookdemo_container').removeClass("hide");
            
            $('.container.bookdemo_container').css("top" , $(window).scrollTop() + 100);
        });
        $('.close_button').click(function(){
            $('.black_bg').addClass("hide");
            $('.bookdemo_container').addClass("hide");
        });
        $('.black_bg').click(function(){
            $('.black_bg').addClass("hide");
            $('.bookdemo_container').addClass("hide");
        });
    });
</script>
</body>
</html>
