<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Colorist
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php if( get_theme_mod('apple_touch') ) : ?>
	<!-- For first- and second-generation iPad: -->
	<link rel="apple-touch-icon" href="<?php echo esc_url( get_theme_mod( 'apple_touch' ) ); ?>">
<?php endif; ?>

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    
    <?php
        wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css',false,'1.1','all');
        wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css',false,'1.1','all');
    
        wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array ( 'jquery' ), 1.1, true);
    ?>
    
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id='woobox-root'></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//woobox.com/js/plugins/woo.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'woobox-sdk'));</script>

    <div class="gototop" id="myBtn">
        <button onclick="topFunction()" >
            <span class="glyphicon glyphicon-menu-up"></span>
        </button>
    </div>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    
<div id="page" class="hfeed site <?php echo colorist_site_style_class(); ?>">

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'colorist' ); ?></a>
	<?php do_action('colorist_before_header'); ?>
	    <header id="masthead" class="site-header <?php echo colorist_site_style_header_class(); ?>" role="banner">
	        <?php do_action('colorist_before_branding'); ?>

			<div id="header" class="<?php  if(!is_front_page()) echo 'fixed permanent';  ?>">
		    <div class="header-content clearfix">
					<?php the_custom_logo(); ?>
		      <nav class="navigation" role="navigation">
						<?php /* Primary navigation */
						wp_nav_menu( array(
						  'menu' => 'top_menu',
						  'depth' => 2,
						  'container' => false,
						  'menu_class' => 'nav',
						  //Process nav menu using our custom nav walker
						  'walker' => new wp_bootstrap_navwalker())
						);
						?>
		        <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> - 1.800.604.2526</p>
		      </nav>
		      <a href="#" class="nav-toggle">Menu<span></span></a> </div>
		  </div>

	    </header><!-- #masthead -->
	<?php do_action('colorist_after_header'); ?>


	<?php if ( function_exists( 'is_woocommerce' ) ||  function_exists( 'is_cart' ) ||  function_exists( 'is_checkout' ) ) :
	 if ( is_woocommerce() || is_cart() || is_checkout() ) { ?>
		<div class="breadcrumb-wrap">
			<div class="container">
				<div class="six columns">
					<?php $breadcrumb = get_theme_mod( 'breadcrumb',true );
					if( $breadcrumb ) : ?>
						<div id="breadcrumb" role="navigation">
							<?php woocommerce_breadcrumb(); ?>
						</div>
					<?php else:  ?>
						&nbsp;
					<?php endif; ?>
				</div>
				<div class="ten columns">
					<header class="entry-header">
						<h1 class="entry-title"><?php woocommerce_page_title(); ?></h1>
					</header><!-- .entry-header -->
				</div>
			</div>
		</div>
	<?php }
	endif; ?>



      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113862494-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113862494-1');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1343271505819252'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1343271505819252&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->



