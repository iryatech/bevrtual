<?php /* Template Name: contactform */ ?>
<style>
    .form_contact_custom{
        padding:100px 0px;
        max-width:900px;
        margin: auto !important;
    }
    .form_contact_custom > div{
        padding-top:8px;
        padding-bottom:8px;
    }
    .form_contact_custom > div > input , .form_contact_custom > div > textarea{
        padding: 2px;
        width: 100%;
        padding-left: 12px;
        border: none;
        font-size: 15px;
    }
    .label_custom_form{
        text-transform: capitalize;
        padding-left: 0px !important;
        font-size: 14px;
        font-weight: 600;
    }
    .submit_button_custom_form{
        
    }
    .custom_form_container{
        background-color: #dddddd;
    }
    .label_custom_form sup{
        color:#bd1414;
    }
    input.submit_button_custom_form {
        background-color: #f15722 !important;
        text-transform: capitalize;
        font-size: 18px;
        font-weight: 500 !important;
        border-radius: 7px;
        margin-top: 35px;
    }
    .form_contact_custom .last-child{
        text-align: right;
    }
    .success_messgae {
        border: 1px solid green;
        text-align: center;
        font-size: 15px;
        padding: 3px !important;
        background-color: #aaccaa;
        text-transform: capitalize;
        font-weight: 600;
    }
</style>

<?php
get_header();
        global $wpdb;
    
    ?>
<div class="custom_form_container">

    <form action="" method="post" id="v_form">
        <div class="row form_contact_custom" >
            <?php
    if ( isset( $_POST["submit_form"] ) ) {
        ?>
            <div class="col-xs-12 success_messgae">
                Your query was submitted successfully
            </div>
                <?php
    }
                ?>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">first Name<sup>*</sup></div>
                <input type="text" name="first_name" placeholder="First Name" required />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Last Name<sup>*</sup></div>
                <input type="text" name="last_name" placeholder="Last Name" required />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Company Name</div>
                <input type="text" name="company_name" placeholder="Company Name" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Phone<sup>*</sup></div>
                <input type="text" name="phone" placeholder="Phone" required />
            </div>
            <div class="col-xs-12">
                <div class="label_custom_form col-xs-12">Email<sup>*</sup></div>
                <input type="email" name="email" placeholder="Email" required />
            </div>
            <div class="col-xs-12">
                <div class="label_custom_form col-xs-12">Message</div>
                <textarea name="message" placeholder="Message" ></textarea>
            </div>
            <div class="col-xs-12 last-child">
                <div class="col-xs-12">
                    <input type="submit" name="submit_form"  class="submit_button_custom_form" value="submit" />
                </div>
            </div>
        </div>
    </form>
</div>
    <?php
    // does the inserting, in case the form is filled and submitted
    if ( isset( $_POST["submit_form"] ) ) {
        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $company_name = $_POST["company_name"];
        $phone = $_POST["phone"];
        $email = $_POST["email"];
        $message = $_POST["message"];
        
        $table = $wpdb->prefix . "contact_forms"; 
        $wpdb->insert( 
            $table, 
            array( 
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company_name' => $company_name,
                'phone' => $phone,
                'email' => $email,
                'message' => $message
            ), 
            array( 
                '%s', 
                '%s', 
                '%s', 
                '%d', 
                '%s', 
                '%s' 
            ) 
        );
    }

get_footer();
?>