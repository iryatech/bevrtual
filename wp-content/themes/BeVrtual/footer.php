<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package BeVrtual
 */
?>

<section id="footer" class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-4">
        <p>BeVrtual is a Digital Agency offering payment plans for all services, making it effortless for small and large businesses to stay at the forefront of their Industry. We offer comprehensive Website Design, Redesign, Web Development, Custom Web Programming, Database Design, Implementation of Ecommerce Platforms, Strategic Marketing and much more.</p>
        <p><span class="icon icon-envelope"></span> Info@BeVrtual.com</p>
        <p><span class="icon icon-phone"></span> &nbsp;&nbsp;(800) 604-2526</p>
      </div>
      <div class="col-1 vr d-none d-sm-block">
      </div>
      <div class="col-6 col-sm-3 offset-sm-1">
        <h5>locations</h5>
        <h6>Los Angeles (Headquarters)</h6>
        <p>1800 Century Park East 600</p>
        <p>Los Angeles, CA 90067</p>
        <h6>San Francisco</h6>
        <p>71 Stevenson Street 400</p>
        <p>San Francisco, CA 94105</p>
      </div>
      <div class="col-6 col-sm-3">
        <h5>&nbsp;</h5>
        <h6>Austin</h6>
        <p>2021 Guadalupe Street 260</p>
        <p>Austin, TX 78705</p>
        <h6>Manhattan</h6>
        <p>415 Madison Avenue fl 14</p>
        <p>Manhattan, NY 10017</p>
        <h6>International</h6>
        <p>Canada, UK</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <hr>
      </div>
      <div class="col-12 col-sm-6">
        <ul class="list-inline">
          <li class="list-inline-item">privacy policy</li>
          <li class="list-inline-item">contact</li>
        </ul>
      </div>
      <div class="col-12 col-sm-6 text-center text-sm-right">
        <p>Copyright © 2018 Be Vrtual, Inc. All Rights Reserved</p>
      </div>
    </div>
  </div>
</section>

<!--
///////////////////////////////
  Mohit's code begins below
//////////////////////////////
-->

<div class="black_bg hide"></div>
<div class="container bookdemo_container hide">
  <div class="row">
    <div class="col-12">
      <div class="close_button">X</div>
    </div>
  </div>
    <div class="submitted_demo" style="display:none">
        <div>thanks!</div>
        <div>please look out for and email from us.</div>
        <div class="close_button">Close window</div>
    </div>
    <div class="row bookdemo_container_row">
        <div class="col-12 bookdemo_tabs">
            <div class="row bookdemo_tabs_container">
                <div class="col-4 options_tab option_one"><p>Step<br>01</p></div>
                <div class="col-4 options_tab option_two"><p>Step<br>02</p></div>
                <div class="col-4 options_tab option_three"><p>Step<br>03</p></div>
            </div>
        </div>
        <div class="col-12">
          <div class="row bookdemo_content">
              <form method="post" class="democlass" id="bookdemoform" action="">
                  <div class="col-12 options_content option_one_content">
                      <div class="row form_one">
                          <div class="col-12">
                              <label for="">Do you have an existing website?</label>
                          </div>
                          <div class="col-12">
                             <!-- <select class="form_one_required website_ask" name="website_ask" required="" tabindex="0" required>
                                  <option value="">Choose</option>
                                  <option value="yes">Yes</option>
                                  <option value="no">No</option>
                              </select>-->
                              <input type="radio" value="yes" class="form_one_required website_ask" name="website_ask" required />Yes
                              <input type="radio" value="no" class="form_one_required website_ask" name="website_ask" required />No
                          </div>
                          <div class="col-12 website_url">
                              <label for="">Current Website URL</label>
                          </div>
                          <div class="col-12 website_url">
                              <input class="website_url_val" type="text" name="website_url" placeholder="Website url..." />
                          </div>
                          <div class="col-12">
                              <label for="">Industry</label>
                          </div>
                          <div class="col-12">
                              <select class="form_one_required" name="industry" required="" tabindex="0" required>
                                  <option value="">Select</option>
                                  <option value="Auto" >Auto</option>
                                  <option value="Leisure" >Leisure</option>
                                  <option value="Finance" >Finance</option>
                                  <option value="Fitness" >Fitness</option>
                                  <option value="Improvement" >Improvement</option>
                                  <option value="Medical" >Medical</option>
                                  <option value="Profit" >Profit</option>
                                  <option value="Other" >Other</option>
                                  <option value="Professional" >Professional</option>
                                  <option value="Estate" >Estate</option>
                                  <option value="Restaurant" >Restaurant</option>
                                  <option value="Retail" >Retail</option>
                                  <option value="Salon" >Salon</option>
                                  <option value="Spa" >Spa</option>
                                  <option value="Travel" >Travel</option>
                              </select>
                          </div>
                          <div class="col-12">
                              <button class="submit_form_one">Continue</button>
                          </div>
                      </div>
                      <div class="row" id="first-tab-bottom">
                        <div class="col-12">
                          <h4>What's In The Proposal?</h4>
                        </div>
                        <div class="col-4 points">
                          <img src="<?php echo get_template_directory_uri().'/media/popup/Optimization_Tips.svg';?>" alt="" class="img-fluid">
                          <p>Optimization Tips</p>
                        </div>
                        <div class="col-4 points">
                          <img src="<?php echo get_template_directory_uri().'/media/popup/Market_Analysis.svg';?>" alt="" class="img-fluid">
                          <p>Market Analysis</p>
                        </div>
                        <div class="col-4 points">
                          <img src="<?php echo get_template_directory_uri().'/media/popup/Custom_Monthly_Pricing.svg';?>" alt="" class="img-fluid">
                          <p>Custom Monthly Pricing</p>
                        </div>
                        <div class="col-12 end">
                          <p>... and so much more!</p>
                        </div>
                      </div>
                  </div>
                  <div class="col-12 options_content option_two_content" style="display:none">
                      <div class="row form_two">
                          <div class="col-12">
                              <label for="">What services are you interested in?</label>
                          </div>
                          <div class="col-12">
<!--                              <select class="form_two_required ask_service" name="service_need" required="" tabindex="0" required>-->
                                  <input type="text" class="form_two_required ask_service" name="service_need" tabindex="0" required />
                                  <input class="service_need_chbx" type="checkbox" value="New Website" />New Website
                                  <input class="service_need_chbx" type="checkbox" value="Website Redesign" />Website Redesign
                                  <input class="service_need_chbx" type="checkbox" value="Online Store" />Online Store
                                  <input class="service_need_chbx" type="checkbox" value="MLS Search (Real Estate)" />MLS Search (Real Estate)
                                  <input class="service_need_chbx" type="checkbox" value="Appointment Booking" />Appointment Booking
                                  <input class="service_need_chbx" type="checkbox" value="Events Calendar" />Events Calendar
                                  <input class="service_need_chbx" type="checkbox" value="Online Food Ordering – Delivery or Takeout" />Online Food Ordering – Delivery or Takeout
                                  <input class="service_need_chbx" type="checkbox" value="Client Portal" />Client Portal
                                  <input class="service_need_chbx" type="checkbox" value="Blog Posting" />Blog Posting
                                  <input class="service_need_chbx" type="checkbox" value="Full or Partial Copywriting" />Full or Partial Copywriting
                                  <input class="service_need_chbx" type="checkbox" value="Social Media Integration" />Social Media Integration
                                  <input class="service_need_chbx service_need_chbx_other" name="service_need_chbx_other" type="checkbox" value="other" />Other
                                  <input class="service_need_chbx" type="checkbox" value="I’m Not Sure – Lets Discuss" />I’m Not Sure – Lets Discuss
                              <!--</select>-->
                          </div>
                          <div class="col-12 other_service">
                              <label for="">Other service</label>
                          </div>
                          <div class="col-12 other_service">
                              <input class="other_service_val" type="text" name="other_service" />
                          </div>
                          <div class="col-12">
                              <button class="submit_form_two">last step ></button>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 options_content option_three_content" style="display:none">
                    <div class="row form_three">
                      <div class="col-12">
                          <label for="">Name</label>
                      </div>
                      <div class="col-12">
                          <input class="form_three_required" type="text" name="name" required />
                      </div>
                      <div class="col-6 special-form">
                          <label for="">Email</label>
                          <input class="form_three_required float-right" type="email" name="email" required />
                      </div>
                      <div class="col-6 special-form">
                          <label for="">Number</label>
                          <input class="form_three_required float-left" type="text" name="number" required />
                      </div>
                      <div class="col-12">
                          <label for="">Any additional Information you would like to include?</label>
                      </div>
                      <div class="col-12">
                          <textarea name="additional_info"></textarea>
                      </div>
                      <div class="col-12">
                          <button type="submit" name="submit_form" class="submit_form_three">Send my free proposal</button>
                      </div>
                    </div>
                    <div class="row align-items-center justify-content-center" id="first-tab-bottom">
                      <div class="col-12">
                        <h4>We get Results</h4>
                      </div>
                      <div class="col-6 col-sm-2 points">
                        <img src="<?php echo get_template_directory_uri().'/media/popup/Increase_Reach.svg';?>" alt="" class="img-fluid">
                      </div>
                      <div class="col-6 col-sm-3 points text-left">
                        <p>Increase Reach</p>
                      </div>
                      <div class="col-6 col-sm-2 points">
                        <img src="<?php echo get_template_directory_uri().'/media/popup/Increase_Conversion.svg';?>" alt="" class="img-fluid">
                      </div>
                      <div class="col-6 col-sm-3 points text-left">
                        <p>Increase Conversion</p>
                      </div>
                      <div class="col-12 end">
                        <p>at affordable monthly pricing ...</p>
                      </div>
                    </div>
                  </div>
              </form>
          </div>
        </div>
    </div>
</div>


<?php

global $wpdb;

if ( isset( $_GET["website_ask"] ) ) {
    //echo "posted";
        $website_ask = $_GET["website_ask"];
        $website_url = $_GET["website_url"];
        $industry = $_GET["industry"];
        $service_need = $_GET["service_need"];
        $other_service = $_GET["other_service"];
        $name = $_GET["name"];
        $email = $_GET["email"];
        $number = $_GET["number"];
        $additional_info = $_GET["additional_info"];
    $table = $wpdb->prefix . "bookdemo";
    
    $wpdb->insert(
        $table,
        array(
            'website_ask' => $website_ask,
            'website_url' => $website_url,
            'industry' => $industry,
            'service_need' => $service_need,
            'other_service' => $other_service,
            'name' => $name,
            'email' => $email,
            'number' => $number,
            'additional_info' => $additional_info
        ),
        array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%d',
            '%s'
        )
    );
    echo "<div id='demoformsubmitconfirmation' style='display:none;'>true</div>";
}else{
    echo "<div id='demoformsubmitconfirmation' style='display:none;'>false</div>";
}
?>
<input type="hidden" value="" class="curr_timestamp_val" />
<?php wp_footer(); ?>

<script type="text/javascript">

$(document).ready(function(){
    
    $('.option_one').click(function(){
        if( $(this).hasClass('filled') ){
            $('.options_content').css("display" , "none");
            $('.option_one_content').css("display" , "block");
        }
    });
    $('.option_two').click(function(){
        if( $(this).hasClass('filled') ){
            $('.options_content').css("display" , "none");
            $('.option_two_content').css("display" , "block");
        }
    });
    $('.option_three').click(function(){
        if( $(this).hasClass('filled') ){
            $('.options_content').css("display" , "none");
            $('.option_three_content').css("display" , "block");
        }
    });
    $('.website_ask').change(function(){
        if( $(this).val() == 'yes'){
            $('.website_url').css("display" , "block");
        }else{
            $('.website_url').css("display" , "none");
        }
    });
    $('.submit_form_one').click(function(){
        var error = 0;
        $('.form_one_required').each(function(){
            if( $(this).val() == '' ){
                error = 1;
                $(this).css("border" , "2px solid red");
            }else{
                $(this).css("border" , "2px solid green");
            }
        });
        if( $('input[name=website_ask]:checked').val() == 'yes'){
            if( $('.website_url_val').val() == '' ){
                error = 1;
                $('.website_url_val').css("border" , "2px solid red");
            }else{
                $('.website_url_val').css("border" , "2px solid green");
            }
        }
            if(error == 0){
                $('.options_content').css("display" , "none");
                $('.option_two_content').css("display" , "block");
                $('.options_tab.option_one').addClass('filled');
                var tab1_filled = 'true';
                var timestamp_id = $('.curr_timestamp_val').val();
                var url = window.location.href+"?tabonefilled=true&timestamp_id="+timestamp_id;
                $.ajax({url: url, success: function(result){
                }});
                
            }else{
                $('.options_tab.option_one').removeClass('filled');
            }
    });
    
    
    $('.service_need_chbx').change(function(){
        var checkbox_value = '';
        $('.service_need_chbx').each(function(){
            if ($(this).is(':checked')) {
                checkbox_value = checkbox_value + " , " + $(this).val();
            }
        });
        $('.ask_service').val(checkbox_value);
    });
    
    $('.submit_form_two').click(function(){
        var error = 0;
        $('.form_two_required').each(function(){
            if( $(this).val() == '' ){
                error = 1;
                $(this).css("border" , "2px solid red");
            }else{
                $(this).css("border" , "2px solid green");
            }
        });

        /*if( $('input[name=service_need_chbx_other]:checked')is(':checked') ){
            if( $('.other_service_val').val() == '' ){
                error = 1;
                $('.other_service_val').css("border" , "2px solid red");
            }else{
                $('.other_service_val').css("border" , "2px solid green");
            }
        }*/
        if( $('.service_need_chbx_other').is(':checked') == true ){
            if( $('.other_service_val').val() == '' ){
                error = 1;
                $('.other_service_val').css("border" , "2px solid red");
            }else{
                $('.other_service_val').css("border" , "2px solid green");
            }
        }
        
            if(error == 0){
                $('.options_content').css("display" , "none");
                $('.option_three_content').css("display" , "block");
                $('.options_tab.option_two').addClass('filled');
                
                var timestamp_id = $('.curr_timestamp_val').val();
                var url = window.location.href+"?tabtwofilled=true&timestamp_id="+timestamp_id;
                $.ajax({url: url, success: function(result){
                }});
            }else{
                $('.options_tab.option_two').removeClass('filled');
            }
    });
    //$('.submit_form_three').click(function(){
    $('#bookdemoform').submit(function(e){
        var error = 0;
        $('.form_three_required').each(function(){
            if( $(this).val() == '' ){
                error = 1;
                $(this).css("border" , "2px solid red");
            }else{
                $(this).css("border" , "2px solid green");
            }
        });
        if(error == 1){
            e.preventDefault();
        }else{
            
            e.preventDefault();
            
            var timestamp_id = $('.curr_timestamp_val').val();
            var url = window.location.href+"?tabthreefilled=true&timestamp_id="+timestamp_id;
            $.ajax({url: url, success: function(result){
            }});
            
            var url = window.location.href+"?"+$("#bookdemoform").serialize();
            $.ajax({url: url, success: function(result){
            }});
            
            $('.submitted_demo').css("display" , "block");
            $('.bookdemo_container_row').css("display" , "none");
            setTimeout(function(){
                $('.submitted_demo').css("display" , "none");
                $('.bookdemo_container_row').css("display" , "block");
                $("#bookdemoform")[0].reset()
                
                $('.options_tab').each(function(){
                    $(this).removeClass('filled');
                });
                $('.options_content').css("display" , "none");
                $('.option_one_content').css("display" , "block");
                
            } , 5000);
            
        }

    });
    
    $('.button_show').click(function(e){
        e.preventDefault();
        $('.black_bg').removeClass("hide");
        $('.bookdemo_container').removeClass("hide");
        $('.container.bookdemo_container').css("top" , $(window).scrollTop() + 100);
        
        var curr_timestamp = $.now();
        $('.curr_timestamp_val').val(curr_timestamp);
        var clicked_from = $(this).attr('id');
        var url = window.location.href+"?analysistimestamp="+curr_timestamp+"&clickedfrom="+clicked_from;
        $.ajax({url: url, success: function(result){
        }});
    });
    $('.close_button').click(function(){
        $('.black_bg').addClass("hide");
        $('.bookdemo_container').addClass("hide");
        $("#bookdemoform")[0].reset();
        $('.options_tab').each(function(){
            $(this).removeClass('filled');
        });
        $('.options_content').css("display" , "none");
        $('.option_one_content').css("display" , "block");
    });
    $('.black_bg').click(function(){
        $('.black_bg').addClass("hide");
        $('.bookdemo_container').addClass("hide");
        $('.custom_form_container').css("display" , "none");
        $("#bookdemoform")[0].reset();
        $('.options_tab').each(function(){
            $(this).removeClass('filled');
        });
        $('.options_content').css("display" , "none");
        $('.option_one_content').css("display" , "block");
    });
    
    
    $('.contact_show').click(function(){
        $('.black_bg').removeClass("hide");
        $('.custom_form_container').css("display" , "block");
        var top_margin = $(window).scrollTop() + 100;
        $('.custom_form_container').css("top" , top_margin);
    });
    $('.service_need_chbx_other').change(function(){
        if( $(this).is(':checked') == true ){
            $('.other_service').css("display" , "block");
        }else{
            $('.other_service').css("display" , "none");
        }
    });
    
    
    
        $('#v_form').submit(function(e){
            var error = 0;
            $('.form_vform_required').each(function(){
                if( $(this).val() == '' ){
                    error = 1;
                    $(this).css("border" , "2px solid red");
                }else{
                    $(this).css("border" , "2px solid green");
                }
            });
            if(error == 1){
                e.preventDefault();
            }else{

                e.preventDefault();
                var url = window.location.href+"?"+$("#v_form").serialize();
                $.ajax({url: url, success: function(result){
                }});

                $('.success_messgae').css("display" , "block");
                $('#v_form').css("display" , "none");
                setTimeout(function(){
                    $('.success_messgae').css("display" , "none");
                    $('#v_form').css("display" , "block");
                    $("#v_form")[0].reset();
                    $(".form_vform_required").each(function(){
                        $(this).css("border" , "none");
                    });
                    $('.custom_form_container').css("display" , "none");
                    $('.black_bg').addClass("hide");
                } , 5000);

            }

        });
    });
</script>
<?php
        global $wpdb;
    if( isset($_GET['analysistimestamp']) ){
        $timestamp = $_GET["analysistimestamp"];
        $clickedfrom = $_GET["clickedfrom"];
        
        $table = $wpdb->prefix . "bookdemo_analysis";
        $wpdb->insert(
            $table,
            array(
                'timestamp_id' => $timestamp,
                'clicked_on' => $clickedfrom
            ),
            array(
                '%s',
                '%s'
            )
        );
    }
    if( isset($_GET['tabonefilled']) ){
        $tabonefilled = $_GET["tabonefilled"];
        $timestamp_id = $_GET["timestamp_id"];
        
        $table = $wpdb->prefix . "bookdemo_analysis";
        $wpdb->query($wpdb->prepare("UPDATE $table SET tab1_filled='$tabonefilled' WHERE timestamp_id=$timestamp_id"));
    }
    if( isset($_GET['tabtwofilled']) ){
        $tabtwofilled = $_GET["tabtwofilled"];
        $timestamp_id = $_GET["timestamp_id"];
        
        $table = $wpdb->prefix . "bookdemo_analysis";
        $wpdb->query($wpdb->prepare("UPDATE $table SET tab2_filled='$tabtwofilled' WHERE timestamp_id=$timestamp_id"));
    }
    if( isset($_GET['tabthreefilled']) ){
        $tabthreefilled = $_GET["tabthreefilled"];
        $timestamp_id = $_GET["timestamp_id"];
        
        $table = $wpdb->prefix . "bookdemo_analysis";
        $wpdb->query($wpdb->prepare("UPDATE $table SET tab3_filled='$tabthreefilled' WHERE timestamp_id=$timestamp_id"));
    }
?>
<div class="contact_show">show</div>
<!--
////////////////////////
Mohit's code ends here
///////////////////////
-->

<script type="text/javascript">

var headerBgs;
window.onload = function () {
  headerBgs = document.getElementsByClassName('header-background');
  if (headerBgs.length === 0) {
    $("nav").addClass("scrolled");
  }
}

//////* ONE DESIGN, FOR ALL DEVICES *///////
(function(){var a=function(a){return document.querySelector(a)},t=document.querySelectorAll(".monitorContentGroup path");a(".monitorGroup");var f=a(".monitorScreen"),g=a(".monitorLogo"),h=a(".monitorStand"),u=a(".monitorStandShadow"),c=a(".monitorBottom"),v=a(".monitorEdge"),C=document.querySelectorAll(".laptopContentGroup path"),w=a(".laptopContentGroup"),x=a(".laptopGroup"),d=a(".laptopScreen"),k=a(".laptopEdgeLeft"),l=a(".laptopEdgeRight"),m=a(".laptopTrackpad"),n=a(".laptopBase");document.querySelectorAll(".tabletContentGroup path");
var e=a(".tabletContentGroup"),b=a(".tabletGroup"),p=a(".tabletButton"),q=a(".tabletCamera"),y=a(".tabletScreen"),r=a(".phoneGroup"),z=a(".phoneButton"),A=a(".phoneCamera"),a=a(".phoneSpeaker");TweenMax.set([c],{transformOrigin:"50% 100%"});TweenMax.set([h,n,y],{transformOrigin:"50% 0%"});TweenMax.set([g,f,d,m,b,x,p,q,e,z,A,a,w,r],{transformOrigin:"50% 50%"});TweenMax.set([k,l],{transformOrigin:"0% 100%"});TweenMax.set(b,{rotation:-90});var B=new TimelineMax({repeat:-1,delay:1,yoyo:!1,paused:!1});
B.timeScale(3);B.from(c,1,{scaleY:0,ease:Power1.easeOut}).from(h,1,{y:-70,ease:Power1.easeOut},"-=1").from(u,.5,{alpha:0,ease:Power1.easeIn},"-=0.5").from(v,1,{y:330},"-=0.25").from(f,2,{y:330,ease:Power1.easeOut},"-=1").staggerFrom(t,1,{scaleX:0},.1,"-=0.51").from(g,1,{scale:0,ease:Back.easeOut.config(2)}).staggerTo(t,1,{alpha:0,delay:2},.1).to(f,1,{y:330,ease:Power1.easeIn},"-=1").to(v,1,{y:330,ease:Power1.easeIn},"-=0.75").to(c,1,{scaleX:.69,y:-23}).to(c,1,{scaleY:.45,alpha:1},"-=1").set(c,{alpha:0}).to(g,
.5,{scale:0,ease:Back.easeIn},"-=1").to(h,1,{y:-120},"-=1.5").to(u,.5,{alpha:0},"-=1.5").from(n,1,{alpha:0},"-=1").from(m,1,{scaleX:0},"-=1").from(d,1,{scale:0,ease:Back.easeOut.config(.5)}).from(k,2,{skewX:-40,scaleY:0,ease:Power3.easeOut},"-=2").from(l,2,{skewX:40,scaleY:0,ease:Power3.easeOut},"-=2").staggerFrom(C,1,{scaleX:0},.1).to(m,.3,{alpha:0,delay:2}).to(d,1,{scaleX:.67}).to(d,1,{scaleY:.8},"-=1").to(w,1,{alpha:0,scale:.5},"-=1").to(n,1,{y:-20,scaleX:0},"-=1").to(k,1,{x:40,transformOrigin:"50% 50%",
scaleY:.85},"-=1").to(l,1,{x:-40,transformOrigin:"50% 50%",scaleY:.85},"-=1").set(x,{alpha:0}).from(b,1,{scale:1.1,alpha:0},"-=1").to(b,2,{rotation:0,delay:2,ease:Anticipate.easeOut}).staggerFrom([p,q],.5,{scale:0,ease:Back.easeOut},.1,"-=1").from(e,2,{rotation:90,scaleX:1.33,scaleY:.8,ease:Power3.easeInOut},"-=0").to([p,q],.5,{scale:0,delay:2}).to(b,1,{scaleX:.45}).to(b,1,{scaleY:.7},"-=1").to(e,1,{y:-5},"-=1").to(y,.5,{scaleY:.92,y:4},"-=0.5").set(r,{alpha:1}).set([b,e],{alpha:0}).staggerFrom([z,
A,a],1,{scale:0,ease:Back.easeOut},.1).to(r,2,{y:80,delay:2,ease:Back.easeIn.config(2)});TweenMax.set("svg",{visibility:"visible"})})();

/////////* Rocket Man */////////
var jetBubbles = document.getElementsByClassName('jetBubble');
var rocketManSVG = document.querySelector('.rocketManSVG');
var shakeGroup = document.querySelector('.shakeGroup');
var star = document.querySelector('.star');
var satellite = document.querySelector('.satellite');
var astronaut = document.querySelector('.astronaut');
var starContainer = document.querySelector('.starContainer');
var badgeLink = document.querySelector('#badgeLink');

TweenMax.to(astronaut, 0.05, {
  y:'+=4',
  repeat:-1,
  yoyo:true
})
var mainTimeline = new TimelineMax({repeat:-1}).seek(100);
var mainSpeedLinesTimeline = new TimelineMax({repeat:-1, paused:false});

mainTimeline.timeScale(2);

function createJets(){
  TweenMax.set(jetBubbles, {
    attr:{
      r:'-=5'
    }
  })
 //jet bubbles
  for(var i = 0; i < jetBubbles.length; i++){
    var jb = jetBubbles[i];
    var tl = new TimelineMax({repeat:-1,repeatDelay:Math.random()}).timeScale(4);
    tl.to(jb, Math.random() + 1 , {
      attr:{
        r:'+=15'
      },
      ease:Linear.easeNone
    })
    .to(jb, Math.random() + 1 , {
      attr:{
        r:'-=15'
      },
      ease:Linear.easeNone
    })

    mainTimeline.add(tl, i/4)
  }
  //speed lines
	for(var i = 0; i < 7; i++){
    var sl = document.querySelector('#speedLine' + i);

    var stl = new TimelineMax({repeat:-1, repeatDelay:Math.random()});
    stl.set(sl, {
      drawSVG:false
    })
    .to(sl, 0.05, {
      drawSVG:'0% 30%',
      ease:Linear.easeNone
    })
    .to(sl, 0.2, {
      drawSVG:'70% 100%',
      ease:Linear.easeNone
    })
    .to(sl, 0.05, {
      drawSVG:'100% 100%',
      ease:Linear.easeNone
    })
     .set(sl, {
      drawSVG:'-1% -1%'
    });

    mainSpeedLinesTimeline.add(stl, i/23);
}
  //stars
	for(var i = 0; i < 7; i++){

    var sc = star.cloneNode(true);
    starContainer.appendChild(sc);
    var calc = (i+1)/2;

    TweenMax.fromTo(sc, calc, {
      x:Math.random()*600,
      y:-30,
      scale:3 - calc
    }, {
      y:(Math.random() * 100) + 600,
      repeat:-1,
      repeatDelay:1,
      ease:Linear.easeNone
    })
  }

  rocketManSVG.removeChild(star);
}


var satTimeline = new TimelineMax({repeat:-1});
satTimeline.to(satellite, 12, {
  rotation:360,
  transformOrigin:'50% 50%',
  ease:Linear.easeNone
})

TweenMax.staggerTo('.pulse', 0.8, {
  alpha:0,
  repeat:-1,
  ease:Power2.easeInOut,
  yoyo:false
}, 0.1);

TweenMax.staggerTo('.satellitePulse', 0.8, {
  alpha:0,
  repeat:-1,
  ease:Power2.easeInOut,
  yoyo:false
}, 0.1)

createJets();
//TweenMax.globalTimeScale(0.50)


/////////* our process */////////
(function() {
  window.signature = {
    initialize: function() {
      return $('.signature g').each(function() {
        var delay, i, len, length, path, paths, previousStrokeLength, results, speed;
        paths = $('path, circle, rect', this);
        delay = 0;
        results = [];
        for (i = 0, len = paths.length; i < len; i++) {
          path = paths[i];
          length = path.getTotalLength();
          previousStrokeLength = speed || 0;
          speed = length < 100 ? 20 : Math.floor(length);
          delay += previousStrokeLength + 100;
          results.push($(path).css('transition', 'none').attr('data-length', length).attr('data-speed', speed).attr('data-delay', delay).attr('stroke-dashoffset', length).attr('stroke-dasharray', length + ',' + length));
        }
        return results;
      });
    },
    animate: function() {
      return $('.signature g').each(function() {
        var delay, i, len, length, path, paths, results, speed;
        paths = $('path, circle, rect', this);
        results = [];
        for (i = 0, len = paths.length; i < len; i++) {
          path = paths[i];
          length = $(path).attr('data-length');
          speed = $(path).attr('data-speed');
          delay = $(path).attr('data-delay');
          results.push($(path).css('transition', 'stroke-dashoffset ' + speed + 'ms ' + delay + 'ms linear').attr('stroke-dashoffset', '0'));
        }
        return results;
      });
    }
  };

  $(document).ready(function() {
    window.signature.initialize();
		var blur = document.getElementById("blur");
    var fadeBlur;

    function startFade() {
      fadeBlur = setInterval(function(){ fadeTimer() },  100);
    }
    function fadeTimer() {
      blur.setAttribute("stdDeviation", blur.getAttribute("stdDeviation") - .1);
      if (blur.getAttribute("stdDeviation") < .09) {
        blur.setAttribute("stdDeviation", 0);
        clearInterval(fadeBlur);
      }
    }
    $('.animation').addClass('active');
    startFade();

    $('button').on('click', function() {
      $('.animation').removeClass('active');
      blur.setAttribute("stdDeviation", 3);

      window.signature.initialize();
      return setTimeout(function() {
        $('.animation').addClass('active');
        startFade();
        return window.signature.animate();
      }, 500);
    });
  });

  $(window).on('load', function() {
    return window.signature.animate();
  });

}).call(this);



window.onload = function () {
  $('.work-box').fancybox();

  let headerHeight;
  let videoHeight = headerBgs[0].clientHeight;
  let gifHeight = headerBgs[1].clientHeight;
  headerHeight = videoHeight;
  if (videoHeight === 0) { headerHeight = gifHeight; }
  //document.getElementById('header-content').setAttribute("style","height:"+headerHeight+"px");

  let imgHeight = document.getElementById('industries-background').clientHeight;
  //document.getElementById('industries-row').setAttribute("style","height:"+imgHeight+"px");

  let navHeight = document.getElementsByTagName('nav')[0].clientHeight;
  $(window).scroll(function(){
    console.log(videoHeight);
    if (videoHeight > 0) {
      var scroll = $(window).scrollTop();
      if (scroll > (videoHeight-navHeight)) { $("nav").addClass("scrolled"); }
  	  else{ $("nav").removeClass("scrolled"); }
    }
  });

}


</script>

</body>
</html>
