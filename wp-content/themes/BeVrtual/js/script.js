//////* ONE DESIGN, FOR ALL DEVICES *///////
(function(){var a=function(a){return document.querySelector(a)},t=document.querySelectorAll(".monitorContentGroup path");a(".monitorGroup");var f=a(".monitorScreen"),g=a(".monitorLogo"),h=a(".monitorStand"),u=a(".monitorStandShadow"),c=a(".monitorBottom"),v=a(".monitorEdge"),C=document.querySelectorAll(".laptopContentGroup path"),w=a(".laptopContentGroup"),x=a(".laptopGroup"),d=a(".laptopScreen"),k=a(".laptopEdgeLeft"),l=a(".laptopEdgeRight"),m=a(".laptopTrackpad"),n=a(".laptopBase");document.querySelectorAll(".tabletContentGroup path");
var e=a(".tabletContentGroup"),b=a(".tabletGroup"),p=a(".tabletButton"),q=a(".tabletCamera"),y=a(".tabletScreen"),r=a(".phoneGroup"),z=a(".phoneButton"),A=a(".phoneCamera"),a=a(".phoneSpeaker");TweenMax.set([c],{transformOrigin:"50% 100%"});TweenMax.set([h,n,y],{transformOrigin:"50% 0%"});TweenMax.set([g,f,d,m,b,x,p,q,e,z,A,a,w,r],{transformOrigin:"50% 50%"});TweenMax.set([k,l],{transformOrigin:"0% 100%"});TweenMax.set(b,{rotation:-90});var B=new TimelineMax({repeat:-1,delay:1,yoyo:!1,paused:!1});
B.timeScale(3);B.from(c,1,{scaleY:0,ease:Power1.easeOut}).from(h,1,{y:-70,ease:Power1.easeOut},"-=1").from(u,.5,{alpha:0,ease:Power1.easeIn},"-=0.5").from(v,1,{y:330},"-=0.25").from(f,2,{y:330,ease:Power1.easeOut},"-=1").staggerFrom(t,1,{scaleX:0},.1,"-=0.51").from(g,1,{scale:0,ease:Back.easeOut.config(2)}).staggerTo(t,1,{alpha:0,delay:2},.1).to(f,1,{y:330,ease:Power1.easeIn},"-=1").to(v,1,{y:330,ease:Power1.easeIn},"-=0.75").to(c,1,{scaleX:.69,y:-23}).to(c,1,{scaleY:.45,alpha:1},"-=1").set(c,{alpha:0}).to(g,
.5,{scale:0,ease:Back.easeIn},"-=1").to(h,1,{y:-120},"-=1.5").to(u,.5,{alpha:0},"-=1.5").from(n,1,{alpha:0},"-=1").from(m,1,{scaleX:0},"-=1").from(d,1,{scale:0,ease:Back.easeOut.config(.5)}).from(k,2,{skewX:-40,scaleY:0,ease:Power3.easeOut},"-=2").from(l,2,{skewX:40,scaleY:0,ease:Power3.easeOut},"-=2").staggerFrom(C,1,{scaleX:0},.1).to(m,.3,{alpha:0,delay:2}).to(d,1,{scaleX:.67}).to(d,1,{scaleY:.8},"-=1").to(w,1,{alpha:0,scale:.5},"-=1").to(n,1,{y:-20,scaleX:0},"-=1").to(k,1,{x:40,transformOrigin:"50% 50%",
scaleY:.85},"-=1").to(l,1,{x:-40,transformOrigin:"50% 50%",scaleY:.85},"-=1").set(x,{alpha:0}).from(b,1,{scale:1.1,alpha:0},"-=1").to(b,2,{rotation:0,delay:2,ease:Anticipate.easeOut}).staggerFrom([p,q],.5,{scale:0,ease:Back.easeOut},.1,"-=1").from(e,2,{rotation:90,scaleX:1.33,scaleY:.8,ease:Power3.easeInOut},"-=0").to([p,q],.5,{scale:0,delay:2}).to(b,1,{scaleX:.45}).to(b,1,{scaleY:.7},"-=1").to(e,1,{y:-5},"-=1").to(y,.5,{scaleY:.92,y:4},"-=0.5").set(r,{alpha:1}).set([b,e],{alpha:0}).staggerFrom([z,
A,a],1,{scale:0,ease:Back.easeOut},.1).to(r,2,{y:80,delay:2,ease:Back.easeIn.config(2)});TweenMax.set("svg",{visibility:"visible"})})();

/////////////////////////////////////////////
/////////* CONTRACT IN, MONEY OUT */////////
///////////////////////////////////////////
/*
var container = document.querySelector('.monimate-container');
var pageGroup = document.querySelector('.pageGroup');
var shredderGroup = document.querySelector('.shredderGroup');
var money = document.querySelector('#money');
var pageMask = document.querySelector('.pageMask');
var mClip = document.querySelector('.mMask');
var lightBlink = document.querySelector('.lightBlink');

//blink the light
TweenMax.to(lightBlink, 0.3, {  fill: '#fff',  repeat: -1,  ease: SteppedEase.config(1),  yoyo: true});
var tl = new TimelineMax({  delay: 0.2,  repeat: -1,});

tl.set(pageMask, {    y: 300  })
  .set(pageGroup, {    y: -300  })
  .set('.strip', {    y: -300  })
  .to(pageMask, 1, {    y: -300,    ease: Power1.easeInOut  })
  .to(pageGroup, 1, {    y: 300,    ease: Power1.easeInOut  }, '-=1')
  .to('.strip', 0.5, {    y: -20,    ease: Power1.easeOut  }, '-=0.5')
  .add("moneydrop")
  .to(mClip, 1.5, {  y: -500,  ease: Power4.easeIn}, 'moneydrop-=1')
  .to(money, 1.5, {  y: 500,  ease: Power4.easeIn}, 'moneydrop-=1');
*/

/////////* Rocket Man */////////
var jetBubbles = document.getElementsByClassName('jetBubble');
var rocketManSVG = document.querySelector('.rocketManSVG');
var shakeGroup = document.querySelector('.shakeGroup');
var star = document.querySelector('.star');
var satellite = document.querySelector('.satellite');
var astronaut = document.querySelector('.astronaut');
var starContainer = document.querySelector('.starContainer');
var badgeLink = document.querySelector('#badgeLink');

TweenMax.to(astronaut, 0.05, {
  y:'+=4',
  repeat:-1,
  yoyo:true
})
var mainTimeline = new TimelineMax({repeat:-1}).seek(100);
var mainSpeedLinesTimeline = new TimelineMax({repeat:-1, paused:false});

mainTimeline.timeScale(2);

function createJets(){
  TweenMax.set(jetBubbles, {
    attr:{
      r:'-=5'
    }
  })
 //jet bubbles
  for(var i = 0; i < jetBubbles.length; i++){
    var jb = jetBubbles[i];
    var tl = new TimelineMax({repeat:-1,repeatDelay:Math.random()}).timeScale(4);
    tl.to(jb, Math.random() + 1 , {
      attr:{
        r:'+=15'
      },
      ease:Linear.easeNone
    })
    .to(jb, Math.random() + 1 , {
      attr:{
        r:'-=15'
      },
      ease:Linear.easeNone
    })

    mainTimeline.add(tl, i/4)
  }
  //speed lines
	for(var i = 0; i < 7; i++){
    var sl = document.querySelector('#speedLine' + i);

    var stl = new TimelineMax({repeat:-1, repeatDelay:Math.random()});
    stl.set(sl, {
      drawSVG:false
    })
    .to(sl, 0.05, {
      drawSVG:'0% 30%',
      ease:Linear.easeNone
    })
    .to(sl, 0.2, {
      drawSVG:'70% 100%',
      ease:Linear.easeNone
    })
    .to(sl, 0.05, {
      drawSVG:'100% 100%',
      ease:Linear.easeNone
    })
     .set(sl, {
      drawSVG:'-1% -1%'
    });

    mainSpeedLinesTimeline.add(stl, i/23);
}
  //stars
	for(var i = 0; i < 7; i++){

    var sc = star.cloneNode(true);
    starContainer.appendChild(sc);
    var calc = (i+1)/2;

    TweenMax.fromTo(sc, calc, {
      x:Math.random()*600,
      y:-30,
      scale:3 - calc
    }, {
      y:(Math.random() * 100) + 600,
      repeat:-1,
      repeatDelay:1,
      ease:Linear.easeNone
    })
  }

  rocketManSVG.removeChild(star);
}


var satTimeline = new TimelineMax({repeat:-1});
satTimeline.to(satellite, 12, {
  rotation:360,
  transformOrigin:'50% 50%',
  ease:Linear.easeNone
})

TweenMax.staggerTo('.pulse', 0.8, {
  alpha:0,
  repeat:-1,
  ease:Power2.easeInOut,
  yoyo:false
}, 0.1);

TweenMax.staggerTo('.satellitePulse', 0.8, {
  alpha:0,
  repeat:-1,
  ease:Power2.easeInOut,
  yoyo:false
}, 0.1)

createJets();

//TweenMax.globalTimeScale(0.50)

/////////* our process */////////
(function() {
  window.signature = {
    initialize: function() {
      return $('.signature g').each(function() {
        var delay, i, len, length, path, paths, previousStrokeLength, results, speed;
        paths = $('path, circle, rect', this);
        delay = 0;
        results = [];
        for (i = 0, len = paths.length; i < len; i++) {
          path = paths[i];
          length = path.getTotalLength();
          previousStrokeLength = speed || 0;
          speed = length < 100 ? 20 : Math.floor(length);
          delay += previousStrokeLength + 100;
          results.push($(path).css('transition', 'none').attr('data-length', length).attr('data-speed', speed).attr('data-delay', delay).attr('stroke-dashoffset', length).attr('stroke-dasharray', length + ',' + length));
        }
        return results;
      });
    },
    animate: function() {
      return $('.signature g').each(function() {
        var delay, i, len, length, path, paths, results, speed;
        paths = $('path, circle, rect', this);
        results = [];
        for (i = 0, len = paths.length; i < len; i++) {
          path = paths[i];
          length = $(path).attr('data-length');
          speed = $(path).attr('data-speed');
          delay = $(path).attr('data-delay');
          results.push($(path).css('transition', 'stroke-dashoffset ' + speed + 'ms ' + delay + 'ms linear').attr('stroke-dashoffset', '0'));
        }
        return results;
      });
    }
  };

  $(document).ready(function() {
    window.signature.initialize();
		var blur = document.getElementById("blur");
    var fadeBlur;

    function startFade() {
      fadeBlur = setInterval(function(){ fadeTimer() },  100);
    }
    function fadeTimer() {
      blur.setAttribute("stdDeviation", blur.getAttribute("stdDeviation") - .1);
      if (blur.getAttribute("stdDeviation") < .09) {
        blur.setAttribute("stdDeviation", 0);
        clearInterval(fadeBlur);
      }
    }
    $('.animation').addClass('active');
    startFade();

    $('button').on('click', function() {
      $('.animation').removeClass('active');
      blur.setAttribute("stdDeviation", 3);

      window.signature.initialize();
      return setTimeout(function() {
        $('.animation').addClass('active');
        startFade();
        return window.signature.animate();
      }, 500);
    });
  });

  $(window).on('load', function() {
    return window.signature.animate();
  });

}).call(this);

/////////* section's height on website load */////////
////////////////////////////////////////////////////
console.log('window');
window.onload = function () {
  console.log('onload');

  $('.work-box').fancybox();

  let headerHeight;
  let videoHeight = headerBgs[0].clientHeight;
  let gifHeight = headerBgs[1].clientHeight;
  headerHeight = videoHeight;
  if (videoHeight === 0) { headerHeight = gifHeight; }
  //document.getElementById('header-content').setAttribute("style","height:"+headerHeight+"px");

  let imgHeight = document.getElementById('industries-background').clientHeight;
  //document.getElementById('industries-row').setAttribute("style","height:"+imgHeight+"px");

  let navHeight = document.getElementsByTagName('nav')[0].clientHeight;
  $(window).scroll(function(){
    console.log('scroll');
    console.log(videoHeight);
    if (videoHeight > 0) {
      var scroll = $(window).scrollTop();
      if (scroll > (videoHeight-navHeight)) { $("nav").addClass("scrolled"); }
  	  else{ $("nav").removeClass("scrolled"); }
    }
  });

}

///////////////////////////////////////////
/////////   Form Handlers   //////////////
/////////////////////////////////////////
