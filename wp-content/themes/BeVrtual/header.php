<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package BeVrtual
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <title>Bevrtual</title>
  <!-- Required meta tags -->
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110081377-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-110081377-1');
</script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<nav class="navbar fixed-top navbar-expand-lg navbar-light">
  <div class="container">
    <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="logo" x="0px" y="0px" viewBox="1.536 171.878 510.464 168.602" enable-background="new 1.536 171.878 510.464 168.602" xml:space="preserve" style="visibility: visible;">
        <g>
          <g id="logo-text">
            <path d="M196.915,227.84v25.088c3.175-6.349,9.626-9.216,16.384-9.216c11.06,0,19.764,7.373,19.764,20.378          c0,13.107-8.704,20.377-19.661,20.377c-6.656,0-13.21-3.379-16.486-9.318v8.705h-3.789V227.84H196.915z M197.018,263.885          c0,9.83,6.86,16.589,16.076,16.589c8.807,0,16.077-5.632,16.077-16.486c0-11.06-7.271-16.589-16.077-16.589          C204.083,247.296,197.018,254.054,197.018,263.885z"></path>
            <path d="M239.001,263.885c0-11.776,8.909-20.377,20.07-20.377c11.06,0,20.992,6.758,19.456,22.221h-35.532          c0.819,9.215,7.885,14.643,16.076,14.643c5.223,0,11.265-2.048,14.234-5.939l2.765,2.253c-3.789,5.018-10.649,7.577-16.999,7.577          C247.91,284.262,239.001,276.48,239.001,263.885z M274.944,262.349c-0.102-9.728-6.451-15.155-15.974-15.155          c-8.09,0-15.155,5.53-16.077,15.155H274.944z"></path>
            <path d="M303.718,271.667l4.608-13.005l5.632-14.438h12.902l-16.896,39.629h-12.595l-16.896-39.629h12.902l5.632,14.438          L303.718,271.667z"></path>
            <path d="M342.016,244.326l0.922,4.301c2.662-3.686,6.144-5.222,10.547-5.222c3.789,0,6.86,1.126,9.626,3.584l-4.813,9.114          c-1.945-1.332-3.891-1.946-6.349-1.946c-4.915,0-9.216,3.174-9.216,9.114v20.479h-11.469v-39.424H342.016z"></path>
            <path d="M383.385,233.472v11.059h9.934v9.523h-10.035v14.848c0,4.198,2.354,5.223,4.505,5.223c1.331,0,3.481-0.308,4.813-0.922          l2.662,9.625c-2.867,1.127-5.223,1.639-8.295,1.742c-8.499,0.408-14.95-3.38-14.95-15.668v-14.848h-7.065v-9.523h7.065v-9.728          L383.385,233.472z"></path>
            <path d="M411.34,244.224v20.788c0,6.041,2.663,9.215,8.09,9.215c5.325,0.104,8.909-4.505,8.909-9.625v-20.377h11.366v39.424          h-10.343l-0.409-5.223c-3.481,4.71-7.987,6.041-12.8,5.939c-8.807,0-16.486-3.994-16.486-19.354v-20.788H411.34z"></path>
            <path d="M479.232,244.224h10.752c0,13.107,0,26.317,0,39.424h-10.343l-0.614-4.916c-2.662,4.199-8.294,5.734-12.083,5.837          c-11.775,0.103-20.377-7.577-20.377-20.582c0-13.107,9.011-20.787,20.685-20.583c4.915,0,9.625,1.946,11.674,5.427          L479.232,244.224z M457.933,263.885c0,6.554,4.607,10.342,10.342,10.342c6.451,0,10.445-5.018,10.445-9.932          c0-5.633-3.584-10.855-10.445-10.855C462.643,253.44,457.933,257.434,457.933,263.885z"></path>
            <path d="M510.464,227.84v55.809h-11.367V227.84H510.464z"></path>
          </g>
          <g id="logo-draw">
            <path d="M151.244,211.763c-0.921,0-1.638,0.717-1.638,1.638v74.957c0,5.939-3.175,11.366-8.295,14.336          L85.504,334.95c-5.12,2.97-11.469,2.97-16.589,0l-55.604-32.153c-0.819-0.41-1.74-0.205-2.253,0.614          c-0.409,0.819-0.204,1.741,0.615,2.253l55.603,32.153c3.072,1.741,6.451,2.663,9.933,2.663c3.38,0,6.861-0.922,9.934-2.663          l55.808-32.256c6.144-3.481,9.933-10.138,9.933-17.203v-74.957C152.883,212.583,152.064,211.763,151.244,211.763z"></path>
            <path d="M68.915,177.357c5.12-2.97,11.469-2.97,16.589,0l54.988,31.846c0.819,0.41,1.741,0.205,2.253-0.614          c0.41-0.819,0.205-1.741-0.614-2.253L87.143,174.49c-6.145-3.482-13.722-3.482-19.764,0l-55.91,32.256          c-6.145,3.481-9.933,10.138-9.933,17.203v74.958c0,0.921,0.717,1.638,1.638,1.638c0.922,0,1.639-0.717,1.639-1.638v-74.958          c0-5.939,3.175-11.366,8.295-14.336L68.915,177.357z"></path>
            <path d="M54.068,295.834c12.697,0,23.142-10.343,23.142-23.143c0-12.801-10.444-23.04-23.142-23.04h-2.253          c-0.717-2.15-2.663-3.687-5.12-3.687c-2.97,0-5.325,2.458-5.325,5.325c0,2.97,2.355,5.325,5.325,5.325          c2.355,0,4.403-1.536,5.12-3.686h2.253c10.956,0,19.865,8.909,19.865,19.865c0,10.957-8.909,19.866-19.865,19.866          c-10.957,0-19.866-8.909-19.866-19.866v-43.52c2.15-0.717,3.687-2.662,3.687-5.12c0-2.97-2.458-5.325-5.325-5.325          s-5.324,2.355-5.324,5.325c0,2.355,1.536,4.403,3.687,5.12v43.418C30.925,285.491,41.267,295.834,54.068,295.834z M46.695,253.337          c-1.127,0-2.048-0.921-2.048-2.048s0.921-2.048,2.048-2.048c1.126,0,2.048,0.921,2.048,2.048          C48.845,252.416,47.923,253.337,46.695,253.337z M32.563,222.105c1.127,0,2.049,0.922,2.049,2.048          c0,1.126-0.922,2.048-2.049,2.048l0,0l0,0c-1.126,0-2.048-0.922-2.048-2.048C30.515,223.129,31.437,222.105,32.563,222.105z"></path>
            <path d="M81.715,256.614c0.308,0,0.614,0,0.922-0.103l12.902,23.962c0.307,0.512,0.819,0.819,1.434,0.819          c0.307,0,0.512-0.103,0.819-0.205c0.819-0.41,1.126-1.434,0.614-2.253l-12.8-23.756c0.921-0.922,1.536-2.253,1.536-3.789          c0-2.97-2.458-5.325-5.325-5.325c-2.97,0-5.325,2.458-5.325,5.325C76.391,254.259,78.745,256.614,81.715,256.614z M81.715,249.242          c1.127,0,2.048,0.921,2.048,2.048s-0.921,2.048-2.048,2.048c-1.126,0-2.048-0.921-2.048-2.048S80.589,249.242,81.715,249.242z"></path>
            <path d="M125.133,256.614c2.97,0,5.324-2.355,5.324-5.325s-2.354-5.325-5.324-5.325s-5.325,2.458-5.325,5.325          c0,1.434,0.614,2.765,1.536,3.789l-16.896,31.129c-0.41-0.103-0.717-0.103-1.127-0.103c-2.97,0-5.325,2.457-5.325,5.324          c0,2.971,2.355,5.326,5.325,5.326s5.325-2.355,5.325-5.326c0-1.33-0.513-2.662-1.434-3.584l16.896-31.231          C124.519,256.614,124.825,256.614,125.133,256.614z M125.133,249.242c1.126,0,2.048,0.921,2.048,2.048s-0.922,2.048-2.048,2.048          c-1.127,0-2.048-0.921-2.048-2.048C122.983,250.163,123.904,249.242,125.133,249.242z M103.322,293.479          c-1.126,0-2.048-0.922-2.048-2.049c0-1.125,0.922-2.047,2.048-2.047c1.127,0,2.048,0.922,2.048,2.047          C105.369,292.557,104.448,293.479,103.322,293.479z"></path>
          </g>
        </g>
      </svg>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <?php /* Primary navigation */
      wp_nav_menu( array(
        'menu' => 'top_menu',
        'depth' => 2,
        'container' => false,
        'menu_class' => 'navbar-nav ml-auto align-items-top',
        //Process nav menu using our custom nav walker
        'walker' => new wp_bootstrap_navwalker())
      );
      ?>
      <li class="nav-item" id="special-nav">
        <a href="<?php get_home_url(); ?>" class="btn bv-cta button_show">contact us</a>
        <p>1.800.604.2526</p>
      </li>
    </div>
  </div>
</nav>
    
<div class="custom_form_container">

    <div class="col-xs-12 success_messgae" style="display:none">
        Your query was submitted successfully
    </div>
    <form action="" method="post" id="v_form">
            <div class="contact_form_popup">
                <h3>
                    Contact us
                </h3>
            </div>
        <div class="row form_contact_custom" >
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">first Name<sup>*</sup></div>
                <input type="text" name="first_name" class="form_vform_required" placeholder="First Name" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Last Name<sup>*</sup></div>
                <input type="text" name="last_name" class="form_vform_required" placeholder="Last Name" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Company Name</div>
                <input type="text" name="company_name" placeholder="Company Name" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Phone<sup>*</sup></div>
                <input type="text" name="phone" class="form_vform_required" placeholder="Phone" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Email<sup>*</sup></div>
                <input type="email" name="email" class="form_vform_required" placeholder="Email" />
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="label_custom_form col-xs-12">Message</div>
                <textarea name="message" placeholder="Message" ></textarea>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="col-xs-12 submit_form_button_container" >
                    <input type="submit" name="submit_form"  class="submit_button_custom_form" value="submit" />
                </div>
            </div>
        </div>
    </form>
</div>
<?php
    // does the inserting, in case the form is filled and submitted
    if ( isset( $_GET["first_name"] ) ) {
        $first_name = $_GET["first_name"];
        $last_name = $_GET["last_name"];
        $company_name = $_GET["company_name"];
        $phone = $_GET["phone"];
        $email = $_GET["email"];
        $message = $_GET["message"];
        
        $table = $wpdb->prefix . "contact_forms"; 
        $wpdb->insert( 
            $table, 
            array( 
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company_name' => $company_name,
                'phone' => $phone,
                'email' => $email,
                'message' => $message
            ), 
            array( 
                '%s', 
                '%s', 
                '%s', 
                '%d', 
                '%s', 
                '%s' 
            ) 
        );
        
        echo "<div id='contactformsubmitconfirmation' style='display:none;'>true</div>";
    }else{
        echo "<div id='contactformsubmitconfirmation' style='display:none;'>false</div>";
    }
?>
