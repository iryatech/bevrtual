<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'BeVrtual' ),
) );

/**
 * Enqueue scripts and styles.
 */
wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/fonts/stylesheet.css',false,'1.0','all');
wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css',false,'1.0','all');
wp_enqueue_style( 'jquery.fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css',false,'1.0','all');
wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css',false,'1.0','all');

/*
*/

function add_this_script_footer(){

	wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/jquery.min.js', array(), '3.3.1' );
	wp_enqueue_script( 'bootstrap.bundle.min', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array(), '4.0.0' );
	wp_enqueue_script( 'jquery.fancybox.pack', get_template_directory_uri() . '/js/jquery.fancybox.pack.js', array(), '2.1.5' );

	wp_enqueue_script( 'SplitText.min', get_template_directory_uri() . '/js/gsap/utils/SplitText.min.js', array(), '0.5.8' );
	wp_enqueue_script( 'DrawSVGPlugin.min', get_template_directory_uri() . '/js/gsap/plugins/DrawSVGPlugin.min.js', array(), '0.0.11' );
	wp_enqueue_script( 'TweenMax.min', get_template_directory_uri() . '/js/gsap/TweenMax.min.js', array(), '1.20.2' );
	wp_enqueue_script( 'AnticipateEase.min', get_template_directory_uri() . '/js/AnticipateEase.min.js', array(), '1.0.0' );
	wp_enqueue_script( 'helper', get_template_directory_uri() . '/js/helper.js', array(), '1.0.0' );

	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '1.0.6' );

}

add_action('wp_footer', 'add_this_script_footer'); ?>
